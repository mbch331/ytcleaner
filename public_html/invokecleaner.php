<?php

use Mediawiki\DataModel\EditInfo;
use Remko\DataHandler;
use Remko\Log;

require_once(__DIR__ . '/header.inc');
require_once(__DIR__ . '/disallow.php');
require_once(__DIR__ . "/../vendor/autoload.php");

$log = new Log();
$datahandler = new DataHandler();

$nonMainSnak = []; //Empty array for adding non mainsnakdata
$nonMainSnakCounter = 0; //Start with 0;

$file = fopen('includes/ytquery.sparql', 'r');
if (!is_bool($file)) {
    $size = filesize('includes/ytquery.sparql');
    if (is_int($size)) {
        $query = fread($file, $size);
        fclose($file);
        if (is_string($query)) {
            $line = $datahandler->parseData($query);
            if (is_string($line)) {
                $log->logResult($line);
            }
        }
    }
}
