<?php

include_once('errorhandle.php');
require '../vendor/autoload.php';

setlocale(LC_ALL, 'nl_NL');
date_default_timezone_set('Europe/Amsterdam');
ini_set('default_charset', 'UTF-8');
ini_set('display_errors', 1);
$globalFuncs = new \Remko\GlobalFuncs();

if ($globalFuncs->isCli()) {
    die('Webmode only');
}
