<?php

if (empty($_GET) || !isset($_SERVER['HTTP_REFERER'])) {
    header('X-Robots-Tag: noindex');
    die('Illegal access to this script. Only to be used in scripts to redirect to Wikidata');
} else {
    header('X-Robots-Tag: noindex,nofollow');
    header('Location: https://www.wikidata.org/wiki/' . $_GET['wdpage']);
}
