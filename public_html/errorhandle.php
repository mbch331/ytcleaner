<?php

//phpcs:disable PSR1.Files.SideEffects.FoundWithSymbols

/**
 *
 * @global string $header
 * @global string $err_msg
 * @param int $level
 * @param string $message
 * @param string $file
 * @param string $line
 */
function Error_Handle($level, $message, $file, $line): void
{
    global $header, $err_msg;
    $lvl = [1 => "fatale run-time fouten",
        2 => "run-time waarschuwingen (niet fatale fouten)",
        4 => "compile-time parse errors", 8 => "run-time opmerkingen (minder belangrijk dan waarschuwingen)",
        16 => "fatale fouten die gebeuren terwijl PHP opstart",
        32 => "waarschuwingen (niet fatale fouten) die gegeven worden terwijl PHP opstart",
        64 => "fatale compile-time fout", 128 => "compile-time waarschuwingen (niet fatale fouten)",
        256 => "door de gebruiker gegenereerde foutmelding",
        512 => "door de gebruiker gegenereerde waarschuwing",
	1024 => "door de gebruiker gegenereerde opmerking",
	2048 => "strict",
	4096 => "herstelbare fout",
	8192 => "Verouderde code",
    ];
    $header .= "From: Error Master <info@remkodekeijzer.nl>";
    $header .= "\r\nX-mailer: PHP/" . phpversion();
    $header .= "\r\nContent-type: text/plain; charset=utf-8";
    $header .= "\r\nContent-Transfer-Encoding: 8bit";
    $header .= "\r\nImportance: High";
    $header .= "\r\nX-priority: 1 (High)";
    $err_msg .= "Er is een fout opgetreden: $lvl[$level]\r\n";
    $err_msg .= "Bestand: $file\r\n Regel: $line\r\n";
    $err_msg .= "Foutmelding: $message\r\n";
    $err_msg .= "-------------------------------------------------------------------------------";
    $err_msg .= "\r\nRA: " . $_SERVER['REMOTE_ADDR'] . "\r\nREF: ";
    $err_msg .= "\r\nUA: " . $_SERVER['HTTP_USER_AGENT'] . "\r\nSS: " . $_SERVER['SERVER_SOFTWARE']
            . "\r\nHOST " . $_SERVER['HTTP_HOST'] . "\r\n";
    $err_msg .= "-------------------------------------------------------------------------------\r\n";
    error_log($err_msg, 1, 'remko@remkodekeijzer.nl', $header);
    die("<br><span class=\"unnamed1\">Er is een fout op deze pagina opgetreden. De beheerder is al ingelicht.</span>");
}

function switchErrorHandler(callable $errorhandler): void
{
    $old_error_handler = set_error_handler($errorhandler);
}

switchErrorHandler("Error_Handle");
