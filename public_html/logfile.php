<?php
require_once('header.inc');
?>
<!doctype HTML>
<html>
    <head>
    <meta charset="UTF-8">
    <meta name="robots" content="noindex,nofollow">
    <title>YouTube Channel ID cleaner for Wikidata - The logfile</title>
    <meta name="description" content="Simple PHP script that uses Wikidata SPARQL to find incorrect values for
          Wikidata property P2397 and checkes the Google YouTube API to check for the correct values and accordingly
          updates Wikidata using the Wikidata API. The logfile from those actions.">
    <style>
        table {
        border-collapse: collapse; width: 100%;
        }

        table, th, td {
        border: 1px solid black; padding: 5px;
        }
        th {
        background-color: silver; padding: 10px;
        }
    </style>
    </head>
    <body>
    <h1>YouTube Channel ID cleaner for Wikidata - The logfile</h1>
    <?php
    $logfile = 'ytcleaner.tsv';
    if (file_exists($logfile)) {
        try {
            $file = fopen($logfile, 'rb');
            if (!is_bool($file)) {
                $logcontents = [];
                while (!feof($file)) {
                    $logcontents[] = fgetcsv($file, 0, $delimiter = "\t");
                }
                array_pop($logcontents);
                reset($logcontents);
                echo ('<table><thead><th>Date</th><th>Item</th><th>Found value</th>'
                . '<th>Corrected value</th><th>Correction '
                . 'accepted?</th></thead>');
                for ($i = 0; $i < count($logcontents); $i++) { //We don't need the first line
                    $content = $logcontents[$i];
                    if (is_array($content)) {
                        $content = $content[0];
                        $date = DateTime::createFromFormat('Y-m-d H:i T', $content);
                        if (!is_bool($date)) {
                            $date = $date->format("d-m-Y H:i T");
                            echo('<tr><td>' . $date . '</td><td><a href="wd.php?wdpage=' . $content[1] . '" target='
                            . '"_blank" rel="nofollow">' . $content[1] . '</a></td><td>' .
                            htmlentities($content[2]) . '</td><td>' . htmlentities($content[3])
                            . '</td><td>' . htmlentities($content[4]) . '</td></tr>' . "\n");
                        }
                    }
                }
                echo ('</table>');
            }
        } catch (Exception $e) {
            trigger_error('Problem with logfile: ' . $e->getMessage());
        }
    } else {
        echo("<p>No logfile found.</p>\r\n<p><a href=\"index.php\" rel=\"root\">Return to the main page</a></p>");
    }
    ?>
    </body>
</html>
