<?php

/*
 * Copyright (C) 2018 Remko
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Remko;

/**
 * functions that use external connections
 *
 * @author Remko
 */
class External
{

    /**
     * Fetch a URL using CURL and return the results
     *
     * @param string $url URL to be opened
     * @param bool $trans Return de transferred data
     * @param int $post GET (0) or POST (1)
     * @param bool $close Does the connection needs to be closed?
     * @param string $postdata
     * @param bool $error Does curl need to fail when an error gets returned
     * @param bool $follow Does curl need to follow redirects?
     * @return string|void The transferred data if requested
     */
    public function invokeURL(
        $url,
        $trans = true,
        $post = 0,
        $close = true,
        $postdata = "",
        $error = true,
        $follow = true
    ) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, $trans);
        curl_setopt($ch, CURLOPT_POST, $post);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mbchbot/0.1 '
                . '(https://www.wikidata.org/wiki/User:Mbch331; mbch331.wikipedia@gmail.com)'
                . ' PHP/' . PHP_VERSION);
        curl_setopt($ch, CURLOPT_COOKIEFILE, 'cookiefile.tmp');
        curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookiefile.tmp');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_FAILONERROR, $error);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, $follow);
        /* @phpstan-ignore-next-line */
        curl_getinfo($ch);
        if ($post == 1) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
        }
        $data = curl_exec($ch);
        if (is_string($data)) {
            return $data;
        } else {
            if (curl_getinfo($ch, CURLINFO_HTTP_CODE) == 404) {
                return "";
            } else {
                trigger_error(curl_error($ch) . ": $url");
            }
        }
        if ($close) {
            curl_close($ch);
        }
    }

    /**
     * Get YouTube data using Google YouTube Api
     *
     * @param string $userid
     * @param string $key
     * @return string
     */
    public function checkYouTube($userid, &$key)
    {
        $url = "https://www.googleapis.com/youtube/v3/channels?part=id&"
                . "forUsername=$userid&fields=items%2Fid&key=$key";
        $jsonres = $this->invokeURL($url, true,0,true,"",true, false);
        if (is_string($jsonres)) {
            $jsondata = (json_decode($jsonres, true));
            if (isset($jsondata['items'][0]['id'])) {
                $returntext = $jsondata['items'][0]['id'];
            } else {
                $returntext = "";
            }
            return $returntext;
        }
        return '';
    }
}
