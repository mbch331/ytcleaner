<?php

/*
 * Copyright (C) 2018 Remko
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Remko;

use Addwiki\Mediawiki\Api\Client\Action\ActionApi;
use Addwiki\Mediawiki\Api\Client\Action\Exception\UsageException as MwUsageException;
use Addwiki\Mediawiki\Api\Client\Auth\UserAndPassword;
use Addwiki\Mediawiki\DataModel\EditInfo;
use Addwiki\Mediawiki\DataModel\Revision;
use Addwiki\Wikibase\Api\WikibaseFactory;
use DataValues\Deserializers\DataValueDeserializer;
use DataValues\Serializers\DataValueSerializer;
use DataValues\StringValue;
use DOMDocument;
use Remko\External;
use Remko\Log;
use Wikibase\DataModel\Entity\PropertyId;
use Wikibase\DataModel\Snak\PropertyValueSnak;
use Wikibase\DataModel\Statement\Statement;

/**
 * Description of DataHandler
 *
 * @author Remko
 */
class DataHandler
{

    /**
     *
     * @var string $channelid
     */
    protected $channelid;

    /**
     *
     * @var WikibaseFactory
     */
    protected $services;

    /**
     *
     * @var Editinfo
     */
    protected $editinfo;

    /**
     *
     * @var External
     */
    protected $external;

    /**
     *
     * @var Log
     */
    protected $log;

    /**
     *
     * @var DOMDocument
     */
    protected $domdocument;

    /**
     *
     * @var string
     */
    protected $resultbody;

    /**
     * @var ActionApi
     */
    protected $api;

    /*
     * Constructor
     *
     * @var External external
     * @var MediaWikiApi api Wikidata Api connection
     * @var Log log Logmodule
     * @var Domdocument domdocument XML Domdocument
     * @return void
     *
     */

    public function __construct()
    {
        $this->external = new External();
        //$this->api = new MediawikiApi("https://www.wikidata.org/w/api.php");
        $this->log = new Log();
        $this->domdocument = new DOMDocument();
        $this->resultbody = '';
        libxml_use_internal_errors(true); //Otherwise loadHTML will fail on incorrect markup
    }

    /**
     * Parse the YouTube data and try to get a channel id out of it
     *
     * @param string $ytid
     * @return string channel id if found or else empty string
     */
    public function parseytdata($ytid = "")
    {
        $ytchannel = [];
        $ytmatch = preg_match("/UC[-_A-Za-z0-9]{21}[AQgw]/", $ytid, $ytchannel);
        if ($ytmatch != 0) { //Channel id already present in link
            $this->channelid = $ytchannel[0];
        } elseif (
            (preg_match(
                "/https?:\/\/(?:www\.)?youtube\.com\/watch/i",
                $ytid
            ) != 0) || preg_match('#watch\?#', $ytid) != 0
            || preg_match('#playlist\?#', $ytid) != 0
        ) {
            $this->channelid = ''; //Video or playlist instead of channel
        } else {
            $linkCheck = preg_match("/https?:\/\/(?:www\.)?youtube\.com\/"
                . "(?:channel\/|user\/|c\/)?/i", $ytid);
            if ($linkCheck == 0) {
                $ytid = 'http://www.youtube.com/' . (str_replace('%2F', '/', urlencode($ytid))); //Turn it into an URL
            }
            if (filter_var($ytid, FILTER_VALIDATE_URL)) {
                $ytpage = $this->external->invokeURL($ytid);
            } else {
                $ytpage = '';
            }
            $this->channelid = '""';
            if (is_string($ytpage) && !empty($ytpage)) {
                $this->getChannelIdFromMetadata($ytpage);
            } else {
                $this->channelid = '';
            }
        }
        return $this->channelid;
    }

    /**
     * Parse the returned YouTube page and set the channel id from the metadata
     *
     * @param string $ytpage
     * @return void
     */
    protected function getChannelIdFromMetadata($ytpage)
    {
        $this->domdocument->loadHTML($ytpage);
        $metas = $this->domdocument->getElementsByTagName('meta');
        if (gettype($metas) !== 'object') {
            $this->channelid = '';
        } else {
            foreach ($metas as $meta) {
                if ($meta->getAttribute('property') == 'og:url') {
                    $channelId = $meta->getAttribute('content');
                    if (!empty($channelId)) {
                        $this->channelid = str_replace('https://www.youtube.com/channel/', '', $channelId);
                    }
                }
            }
        }
    }

    /**
     * Get the QID from the Entity url
     *
     * @param string $entity Full entity URL
     * @return string Parsed QID from the entity URL
     */
    public function getQidFromEntity($entity)
    {
        $value = preg_replace(
            '/https?:\/\/www\.wikidata\.org\/entity\//i',
            '',
            $entity
        ) ?? '';
        return $value;
    }

    /**
     *
     * @global string $wduser Username for Wikidata api
     * @global string $wdpass Password for Wikidata api
     * @param EditInfo $editinfo
     * @param string $qid Entity id for Wikidata
     * @param string $guid Guid: Specific revision of a property of an entity
     * @param string $channelid YouTube channel id
     * @param string $origin Original value
     * @param string $type What to do: update or remove
     * @return MwUsageException|bool|null|void Error if any
     */
    public function updateWikidata(
        $editinfo,
        $qid,
        $guid,
        $channelid,
        $origin,
        $type = 'update'
    ) {
        global $wduser, $wdpass;
        require_once(__DIR__ . '/../includes/accountdata.inc');
        $auth = new UserAndPassword($wduser, $wdpass);
        $this->api = new ActionApi("https://www.wikidata.org/w/api.php", $auth);
        $dataValueClasses = ['unknown' => 'DataValues\UnknownValue', 'string' => 'DataValues\StringValue'];
        $this->editinfo = $editinfo;
        $this->services = new WikibaseFactory(
            $this->api,
            new DataValueDeserializer($dataValueClasses),
            new DataValueSerializer()
        );
        $revision = $this->services->newRevisionGetter()->getFromId($qid);
        if (empty($revision)) {
            $this->log->logytc(date("Y-m-d H:i T") . "\t$qid\t$origin\t$channelid\tNo revision" . PHP_EOL);
            $this->resultbody .= '<p>No revision available for ' . $qid . '</p>' . PHP_EOL;
            return false;
        } else {
            $item = $revision->getContent()->getData();
            $statementList = $item->getStatements();
            $statement = $statementList->getByPropertyId(PropertyId::newFromNumber(2397));
            $mainStatement = $statement->getFirstStatementWithGuid($guid);
            $type == 'update' ? $resultToReturn = $this->updateStatement(
                $mainStatement,
                $channelid,
                $revision,
                $origin,
                $guid,
                $qid
            ) : $resultToReturn = $this->removeStatement($revision, $guid, $origin, $channelid);
            return $resultToReturn;
        }
    }

    /**
     *
     * @param Statement $mainStatement
     * @param string $channelid
     * @param Revision $revision
     * @param string $origin
     * @param string $guid
     * @param string $qid
     * @return MwUsageException|void|null Error if any
     */
    protected function updateStatement($mainStatement, $channelid, $revision, $origin, $guid, $qid)
    {
        $mainStatement->setMainSnak(new PropertyValueSnak(
            PropertyId::newFromNumber(2397),
            new StringValue($channelid)
        ));
        $return = $this->saveUpdatedSnak($revision, $qid, $origin, $channelid, $guid);
        return $return;
    }

    /**
     *
     * @param Revision $revision
     * @param string $guid
     * @param string $origin
     * @param string $channelid
     * @return MwUsageException|void|null Error if any
     */
    protected function removeStatement($revision, $guid, $origin, $channelid)
    {
        $qid = preg_replace("/\-([a-zA-Z0-9-])+/i", "", $guid);
        try {
            $removedStatement = $this->services->newStatementRemover()->remove($guid, $this->editinfo);
        } catch (MwUsageException $e) {
            if ($e->getMessage() == '⧼abusefilter-warning-68⧽') {
                $removedStatement = $this->services->newStatementRemover()->remove($guid, $this->editinfo);
            } else {
                $message = json_decode($e->getMessage());
                $this->log->logytc(date("Y-m-d H:i T") . "\t$qid\t$origin\t$channelid\t" . $message->html);
            }
        } catch (\Exception $e) {
            $this->log->logytc(date("Y-m-d H:i T") . "\t$qid\t$origin\t$channelid\t" . $e->getMessage());
        }

        if (is_string($qid)) {
            return $this->saveUpdatedSnak($revision, $qid, $origin, $channelid, $guid);
        }
    }

    /**
     *
     * @param Revision $revision
     * @param string $qid
     * @param string $origin
     * @param string $channelid
     * @param string $guid
     * @return MwUsageException|null|void Error if any
     */
    protected function saveUpdatedSnak($revision, $qid, $origin, $channelid, $guid)
    {
        try {
            $this->services->newRevisionSaver()->save($revision, $this->editinfo);
            return;
        } catch (MwUsageException $e) {
            $qid = preg_replace("/\-([a-zA-Z0-9-])+/i", "", $guid);
            $this->log->logytc(date("Y-m-d H:i T") . "\t$qid\t$origin\t$channelid\t" . $e->getMessage() . PHP_EOL);
            return $e;
        }
    }

    /**
     * Fetch data from Wikidata and return it as an array
     *
     * @param string $query The query to be executed
     * @return array<array> $data Part of the decoded json as an array
     */
    public function fetchData($query = '')
    {
        $json = $this->external->invokeURL('https://query.wikidata.org/bigdata/namespace/wdq/'
            . 'sparql?query=' . urlencode($query) . '&format=json', true);
        if (is_string($json)) {
            $dataWikidata = json_decode($json, true);
            $data = $dataWikidata['results']['bindings'];
        } else {
            $data = [[]];
        }
        return $data;
    }

    /**
     *
     * @param string $query
     * @return string
     */
    public function parseData($query)
    {
        $data = $this->fetchData($query);
        $nonMainSnakCounter = 0;
        $nonMainSnak = [];
        if (count($data) == 0) {
            return "<p>Geen resultaten</p>";
        } else {
            for ($i = 0; $i < count($data); $i++) {
                if (!array_key_exists('item', $data[$i])) {
                    $this->log->logytc(date("Y-m-d H:i T") . print_r($data[$i], true) . "\r\n");
                    continue;
                }
                $id = $this->getQidFromEntity($data[$i]['item']['value']);
                if ($data[$i]['YouTube']['value'] === "UCD4KovqgUQaPuRKOFyMyt") {
                    continue;
                }
                $channelid = $this->parseytdata($data[$i]['YouTube']['value']);
                if ($data[$i]['snak']['value'] != 'mainsnak') { // Need to build handler for ref and qualifier
                    $nonMainSnak[$nonMainSnakCounter]['item'] = $data[$i]['item']['value'];
                    $nonMainSnak[$nonMainSnakCounter]['value'] = $channelid;
                    $nonMainSnak[$nonMainSnakCounter]['type'] = $data[$i]['snak']['value'];
                    $nonMainSnakCounter++;
                    continue;
                }
                $guid = preg_replace("/Q(\d+)-/i", "Q$1\$", str_replace(
                    'http://www.wikidata.org/entity/statement/',
                    '',
                    $data[$i]['YouTubeEntity']['value']
                ));
                if ($channelid != '' && $channelid != $data[$i]['YouTube']['value']) {
                    $editinfo = new EditInfo(
                        $summary = 'Fixing invalid value for [[Property:P2397|P2397]]',
                        $minor = false,
                        $bot = true
                    );
                    if (is_string($guid) && substr($guid, 0, 1) !== "L") {
                        $updateResult = $this->updateWikidata(
                            $editinfo,
                            $id,
                            $guid,
                            $channelid,
                            $data[$i]['YouTube']['value'],
                            'update'
                        );

                        if (is_object($updateResult)) {
                            if (get_class($updateResult) == 'Mediawiki\Api\UsageException') {
                                $errortext = $updateResult->getMessage();
                            } else {
                                $errortext = $updateResult->Result->info;
                            }
                            $this->resultbody .= "<p>Updating <a href=\"http://www.wikidata.org/wiki/$id\">"
                                . "$id</a> failed:'."
                                . "$errortext.'.</p>";
                        } elseif ($updateResult !== false) {
                            $this->resultbody .= "<p>Updating <a href=\"http://www.wikidata.org/wiki/$id\">$id</a> "
                                . "succeeded.</p>";
                        }
                    } elseif (substr($guid, 0, 1) == "L") {
                        $this->resultbody .= "<p>Updating <a href=\"http://www.wikidata.org/wiki/Lexeme:$id\">"
                            . "$id</a> failed: Lexeme.</p>";
                    } else {
                        trigger_error('Guid is not a string: ' . print_r($guid, true));
                    }
                } elseif ($channelid == '') {
                    $editinfo = new EditInfo(
                        $summary = 'Removing unfixable value from [[Property:P2397|P2397]]',
                        $minor = false,
                        $bot = true
                    );
                    if (is_string($guid)) {
                        $updateResult = $this->updateWikidata(
                            $editinfo,
                            $id,
                            $guid,
                            $channelid,
                            $data[$i]['YouTube']['value'],
                            'remove'
                        );
                        if (is_object($updateResult) == 'object') {
                            $errortext = $updateResult->__toString();
                            $this->resultbody .= "<p>Updating <a href=\"http://www.wikidata.org/wiki/$id\">"
                                . "$id</a> failed:'."
                                . "$errortext.'.</p>";
                        } elseif ($updateResult !== false) {
                            $this->resultbody .= "<p>Updating <a href=\"http://www.wikidata.org/wiki/$id\">$id</a> "
                                . "succeeded.</p>";
                        }
                    } elseif (substr($guid, 0, 1) == "L") {
                        $this->resultbody .= "<p>Updating <a href=\"http://www.wikidata.org/wiki/Lexeme:$id\">"
                            . "$id</a> failed: Lexeme.</p>";
                    } else {
                        trigger_error('Guid is not a string');
                    }
                }
                $this->log->logytc(date("Y-m-d H:i T") . "\t$id\t" . $data[$i]['YouTube']['value'] .
                    "\t$channelid\tSucceeded\r\n");
            }
            if (count($nonMainSnak) > 0) {
                $this->log->logNonMainSnakMail($nonMainSnak);
                $this->resultbody .= "<p>Found one or more P2397 links as reference and/or qualifier.</p>";
            }
        }

        if (count($data) > 0) {
            $this->resultbody .= "<p>Finished updating</p>";
        }
        return $this->resultbody;
    }
}

