<?php

/*
 * Copyright (C) 2018 Remko
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Remko;

use Exception;
use PHPMailer\PHPMailer\PHPMailer;

/**
 * Helper class for logging functions
 *
 * @author Remko
 *
 */
class Log
{

    /**
     * Log result of ytcleaner
     *
     * @param string $text Line to be added to the logfile
     * @return void
     */
    public function logytc($text)
    {
        try {
            $file = fopen('ytcleaner.tsv', 'a+');
            if (is_bool($file)) {
                trigger_error('Logfile can\'t be opened');
            } else {
                fwrite($file, $text);
            }
        } catch (Exception $e) {
            trigger_error($e->getMessage());
        }
    }

    /**
     * Function that sends a mail to the admin about Snaks that aren't mainsnaks. These need manual cleaning.
     *
     * @param  array<int, array<string, mixed>> $nonMainSnakMail Array containing data for non mainsnaks
     * @return void
     */
    public function logNonMainSnakMail($nonMainSnakMail)
    {
        $mail = new PHPMailer();
        $mail->isSMTP();
        $mail->Host = 'mail.tools.wmcloud.org';
        $mail->SMTPAuth = false;
        $mail->SMTPDebug = 2;
        $mail->setFrom("tools.ytcleaner@toolforge.org", 'YouTube Channel ID cleaner for Wikidata');
        $mail->addAddress('mbch331.wikipedia@gmail.com', 'Mbch331');
        $mail->addReplyTo('mbch331.wikipedia@gmail.com', 'Mbch331');
        $mail->isHTML(true);
        $mail->CharSet = 'utf-8';
        $mail->Subject = "Non mainsnaks using P2397";
        $bodytable = "<table>\r\n<thead>\r\n<tr>\r\n<th>Item</th><th>Type</th>"
            . "<th>Value</th>\r\n</tr>\r\n</thead>\r\n<tbody>\r\n";
        foreach ($nonMainSnakMail as $value) {
            $bodytable .= "<tr>\r\n<td><a href=\"" . $value['item'] . "\">" .
                $value['item'] . "</a></td><td>" . $value['type'] .
                "</td><td>" . $value['value'] . "</td>\r\n</tr>\r\n";
        }
        $bodytable .= "</tbody>\r\n</table>";
        $body = '<!DOCTYPE html><html><head><meta http-equiv="Content-Type" '
            . 'content="text/html; charset=utf-8"><meta http-equiv='
            . '"X-UA-Compatible" content="IE=edge"><meta name="viewport" '
            . 'content="width=device-width, initial-scale=1.0"></head><body>'
            . '<!--' . count($nonMainSnakMail) . '-->' . $bodytable .
            '</body></html>';
        $mail->msgHTML($body);
        if (!$mail->send()) {
            echo 'Message could not be sent.';
            echo 'Mailer Error: ' . $mail->ErrorInfo;
        } else {
            echo 'Message has been sent';
        }
    }

    /**
     *
     * @param string $bodytext
     */
    public function logResult($bodytext): void
    {
        $mail = new PHPMailer();
        $mail->isSMTP();
        $mail->Host = 'mail.tools.wmcloud.org';
        $mail->SMTPAuth = false;
        $mail->SMTPDebug = 2;
        $mail->setFrom("tools.ytcleaner@toolforge.org", 'YouTube Channel ID cleaner for Wikidata');
        $mail->addAddress('mbch331.wikipedia@gmail.com', 'Mbch331');
        $mail->addReplyTo('mbch331.wikipedia@gmail.com', 'Mbch331');
        $mail->isHTML(true);
        $mail->CharSet = 'utf-8';
        $mail->Subject = "YTCleaner result log";

        $body = '<!DOCTYPE html><html><head><meta http-equiv="Content-Type" '
            . 'content="text/html; charset=utf-8"><meta http-equiv="X-UA-Compatible" '
            . 'content="IE=edge"><meta name="viewport" '
            . 'content="width=device-width, initial-scale=1.0"></head><body>'
            . $bodytext . '</body></html>';
        $mail->msgHTML($body);
        if (!$mail->send()) {
            echo 'Message could not be sent.';
            echo 'Mailer Error: ' . $mail->ErrorInfo;
        } else {
            echo 'Message has been sent';
        }
    }
}
