#!/bin/bash
HOME=/data/project/ytcleaner
#$ -N cron-tools.ytcleaner-1
#$ -M mbch331.wikipedia@gmail.com
#$ -o /dev/null
#$ -e $HOME/log/cron-tools.ytcleaner-1.err
#$ -b y
#$ -sync y

cd $HOME/log
curl -sS "https://ytcleaner.toolforge.org/invokecleaner.php" --user-agent "Mbchbot/0.1" >> $HOME/log/cleaner.log
find . -type f  \( -iname "cleaner.*" ! -iname "cleaner.log" \) -delete
